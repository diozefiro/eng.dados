use dw_igti
go

/*
select top 3 * From pnadc
select top 3 * From mesorregiao

select distinct renda From pnadc
select distinct uf From pnadc order by 1
select distinct graduacao From pnadc
select distinct anosesco From pnadc order by 1
select distinct trab From pnadc
select distinct cor From pnadc
*/

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'pnadc' and COLUMN_NAME = 'renda_trat')
	alter table pnadc add renda_trat decimal(10,1)
Go
update pnadc
	set renda_trat = convert(decimal(10,1),renda/10)
Go

--1
select Convert(decimal(10,1), avg(renda_trat) ) From pnadc

--2
select Convert(decimal(10,1), avg(renda_trat) ) From pnadc where uf = 'Distrito Federal'

--3
select 
	Convert(decimal(10,1), avg(renda_trat) ),
	SUM(idade), 
	COUNT(*), 
	SUM(idade)/COUNT(*),
	COUNT(*)/SUM(idade)
From pnadc a 
where exists (select 1 
			  from mesorregiao b 
			  Where a.uf = b.uf_nome 
			  And b.uf_regiao_nome = 'Sudeste')
Go

-- 4
select uf, Convert(decimal(10,1), avg(renda_trat) ) From pnadc group by uf order by 2 asc

-- 5
select 
	uf,
	--grad_sim = SUM( iif( graduacao = 'sim', 1, 0) ),
	--grad_n�o = SUM( iif( graduacao = 'n�o', 1, 0) ),
	--grad_nf = SUM( iif( graduacao is null, 1, 0) ),
	anos_escol = avg(anosesco),
	total = count(*)
	--mediaescol = convert(float, SUM( iif( graduacao = 'sim', 1, 0) )) / convert(float,count(*) )*100
From pnadc
--where graduacao is not null
group by uf
--order by mediaescol desc
order by anos_escol desc

--7 
select 
	uf,
	--sim = SUM( iif( graduacao = 'sim', 1, 0) ),
	--n�o = SUM( iif( graduacao = 'n�o', 1, 0) ),
	--nf = SUM( iif( graduacao is null, 1, 0) ),
	total = count(*),
	anos_escol = avg(anosesco)--,
	--mediaescol = (convert(float, SUM( iif( graduacao = 'sim', 1, 0) )) / convert(float,count(*) ))*100
From pnadc
where uf = 'Paran�'
and idade between 25 and 30
--and graduacao is not null
group by uf


--8 
select 
	Convert(decimal(10,1), avg(renda_trat) )
From pnadc a 
where exists (select 1 
			  from mesorregiao b 
			  Where a.uf = b.uf_nome 
			  And b.uf_regiao_nome = 'Sul')
And idade between 25 and 35
and trab = 'Pessoas na for�a de trabalho'

-- 9 
select 
	soma_renda = Convert(decimal(10,1), sum(renda_trat) ),
	Convert(decimal(10,1), sum(renda_trat) ) / c.qtd_meso
From pnadc a 
Cross Join (Select COUNT(distinct B.nome) as qtd_meso from mesorregiao b where b.uf_sigla = 'MG') as c
Where a.uf = 'Minas Gerais'
group by qtd_meso

---------------------------------------------------------

Declare @Renda decimal(10,1)
Declare @Qtd_Meso Int 

select @Renda = Convert(decimal(10,1), sum(renda_trat) ) From pnadc a Where a.uf = 'Minas Gerais'
Select @Qtd_Meso = COUNT(distinct B.nome) from mesorregiao b where b.uf_sigla = 'MG'

Select @Renda / @Qtd_Meso

-- 10 
select 
	Convert(decimal(10,1), avg(renda_trat) )
From pnadc a 
where exists (select 1 
			  from mesorregiao b 
			  Where a.uf = b.uf_nome 
			  And b.uf_regiao_nome = 'Norte')
And idade between 25 and 35
And graduacao = 'sim'
and cor in ('Preta', 'Parda')