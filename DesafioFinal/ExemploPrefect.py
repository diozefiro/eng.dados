
# Importando Bibliotecas Necessárias

#from airflow import DAG
#from airflow.operators.bash_operator import BashOperator
#from airflow.operators.python_operator import PythonOperator, BranchPythonOperator

import prefect 
from prefect import task, Flow

@task
def hello_world():
    logger = prefect.context.get("logger")
    logger.info("Hello World do Prefect em Cloud!")

with Flow("Hello World") as flow: #nome do Flow que será exibido na plataforma
    hello_world()
#                      ou
#flow = Flow("hello-flow", tasks=[hello_world])

### para execução local
#flow.run()

# ### para execução na nuvem
flow.register(project_name="IGTIDesafioFinal")
### necessário criar um Agent para execução na nuvem
#flow.run_agent()

