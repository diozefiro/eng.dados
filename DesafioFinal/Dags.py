
# Importando Bibliotecas Necessárias

import prefect 
import json
import boto3
import os

from sqlalchemy import create_engine
from pandas import json_normalize
from prefect import task, Flow

# Decrevendo o Agendamento do Fluxo


# Recuperar dados do IBGE
@task
def recuperaIBGE ():
    import pandas as pd
    import requests 
    response = requests.get("https://servicodados.ibge.gov.br/api/v1/localidades/mesorregioes")
    dataibge = response.json()
    microrregioes = json_normalize(dataibge)
    df = pd.DataFrame(microrregioes)
    path1 = r'C:\ProjetosGitEngDados\eng.dados\DesafioFinal\tmp\microrregiao.csv'
    df.to_csv(path1, index=False, encoding='utf-8', sep=';')
    return path1

# Recuperar dados do MongoDB
@task
def recuperaMongo():
    import pymongo
    import pandas as pd
    conect = pymongo.MongoClient('mongodb+srv://estudante_igti:SRwkJTDz2nA28ME9@unicluster.ixhvw.mongodb.net/ibge?w=majority%20(Links%20para%20um%20site%20externo.)%22)&readPreference=primary', tls=True, tlsAllowInvalidCertificates=True)
    db = conect.ibge
    colect = db.pnadc20203
    filter={'idade': { '$gte': 20, '$lte': 40 }, 'sexo': 'Mulher' }
    #datamongo = conect['ibge']['pnadc20203'].find(filter=filter)
    df = pd.DataFrame(list(colect.find(filter=filter)))
    path2 = r'C:\ProjetosGitEngDados\eng.dados\DesafioFinal\tmp\pnadc20203.csv'
    df.to_csv(path2, index=False, encoding='utf-8', sep=';')
    return path2

# Salvar dados recuperados no S3 da AWS
@task
def salva_dados_s3(path_arquivo):
    aws_access_key_id = 'aaa'
    aws_secret_access_key = 'bbb'

    clientS3 = boto3.client(
        's3', 
        aws_access_key_id = aws_access_key_id,
        aws_secret_access_key = aws_secret_access_key
    )
    clientS3.upload_file(path_arquivo, "igti-bootcamp-ed-2021-869932645853", path_arquivo)
    return path_arquivo
    
# Carregar os dados do S3 no RDS - MySQL


#verificando se as origens estão com dados corretos
#@task
#def exibe_ibge(dataibge):
#    logger = prefect.context.get("logger")
#    logger.info(dataibge)
@task
def exibe_mongo(path2):
    logger = prefect.context.get("logger")
    logger.info(path2)

# Organiza o fluxo de dados
with Flow("Fluxo_Desafio") as flow: #nome do Flow que será exibido na plataforma
 #   ibge = recuperaIBGE()
    mongo = recuperaMongo()
#    i = exibe_ibge(ibge)
    m = exibe_mongo(mongo)

# ### para execução na nuvem
flow.register(project_name="IGTIDesafioFinal")