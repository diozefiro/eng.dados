#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# Para programar em um compilador python e executar os flows na nuvem, é necessário configurar um AGENTs no site do Prefect
# pois é este agent que faz a inteface local com o server na nuvem do prefect
# flow.run_agent() é o comando que já cria um agent e o inicia


# In[ ]:


import prefect 
from prefect task, Flow

@task
def hello_world():
    logger = prefect.context.get("logger")
    logger.info("Hello, Eu nem acredito que consegui habilitar o Prefect Cloud!")

with Flow("hello-flow-desc") as flow:
    hello_world()

flow.register(project_name="IGTI")
flow.run_agent(token="PJk8cV--3KmF1TuWvhMd5Q")


# executar o hello_world.py no terminal - lembrar que estar na pasta correta ou passar o caminho completo

