#!/usr/bin/env python
# coding: utf-8

# In[2]: IMPORTAR AS BIBLIOTECAS
from datetime import datetime, timedelta
import prefect 
from prefect import task, Flow
from prefect.schedules import IntervalSchedule
import pandas as pd
from urllib.request import urlretrieve
#import requests
#from io import StringIO

# In[ ]: COLOCAR INTERVALO DE 1 MINUTO PARA NOVA TENTATIVA EM CASO DE ERRO
retry_delay = timedelta(minutes=1)
# AGENDAR A EXECUÇÃO DO FLOW
agendador = IntervalSchedule(interval=timedelta(minutes=2))

@task
def get_data():
    #url = 'https://raw.githubusercontent.com/A3Data/hermione/master/hermione/file_text/train.csv'
    #rurl = requests.get(url, verify=False)
    #df = pd.read_csv(StringIO(rurl.text))
    df = pd.read_csv("https://raw.githubusercontent.com/A3Data/hermione/master/hermione/file_text/train.csv")
    #url = urlretrieve("https://raw.githubusercontent.com/A3Data/hermione/master/hermione/file_text/train.csv", './titanic.csv')
    #df = pd.read_csv("./titanic.csv")
    return df

@task
def calcula_media_idade(df):
    return df.Age.mean()

@task
def exibe_media_calculada(m):
    logger = prefect.context.get("logger")
    logger.info(f"A média de idade calculada foi {m}")
    
@task
def exibe_dataset(df):
    logger = prefect.context.get("logger")
    logger.info(df.head(3).to_json())


with Flow("Titanic01", schedule=agendador) as flow:
    df = get_data()
    med = calcula_media_idade(df)
    e = exibe_media_calculada(med)
    ed = exibe_dataset(df)
    
flow.register(project_name='Titanic', idempotency_key = flow.serialized_hash() )
flow.run_agent(token="PJk8cV--3KmF1TuWvhMd5Q")

# no terminal do anaconda é possível criar o projeto antes de exeuctar o script python, para evitar erros

##(base) C:\Users\EXYON->prefect create project Titanic --description "Projeto de teste de flows do Titanic"

# toda vez que o terminal Anaconda CMD for finalizado, precisamos fazer novo login com o Runner Token