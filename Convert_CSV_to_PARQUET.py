#Using pip:
pip install pandas pyarrow

#or using conda:
conda install pandas pyarrow -c conda-forge

#Convert CSV to Parquet in chunks
# csv_to_parquet.py

import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq

csv_file = '/path/to/my.csv'
parquet_file = '/path/to/my.parquet'
chunksize = 100_000

csv_stream = pd.read_csv(csv_file, sep='\t', chunksize=chunksize, low_memory=False)

for i, chunk in enumerate(csv_stream):
    print("Chunk", i)
    if i == 0:
        # Guess the schema of the CSV file from the first chunk
        parquet_schema = pa.Table.from_pandas(df=chunk).schema
        # Open a Parquet file for writing
        parquet_writer = pq.ParquetWriter(parquet_file, parquet_schema, compression='snappy')
    # Write CSV chunk to the parquet file
    table = pa.Table.from_pandas(chunk, schema=parquet_schema)
    parquet_writer.write_table(table)

parquet_writer.close()


# We can now read CSV files directly into PyArrow Tables using pyarrow.csv.read_csv. 
# This is probably faster than using the Pandas CSV reader, although it may be less flexible.

# Possível também a leitura com DASK, que é infinitamente mais rapido que Pandas.